# c-sign

### 
- c-sign 电子签名

### c-design交流群号：330647926

### 示例预览

[https://cloud.vuedata.wang/cdesign/#/pages/sign/sign](https://cloud.vuedata.wang/cdesign/#/pages/sign/sign)
####
![微信小程序预览](https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/4616c239-a64e-45d7-a98a-72608578e124.png?x-oss-process=image/resize,m_fixed,w_240)

### 一、使用示例

```html
<template>
	<view>
		<c-sign ref="cSign"  :lineColor="lineColor" :lineWidth="lineWidth" :bgColor='bgColor'></c-sign>
		<view class="content">
			<view>画笔设置</view>
			<view class="btnBox">
				<button @click="lineColor='#000000'" size="mini">黑色画笔</button>
				<button @click="lineColor='#f44'" size="mini">红色画笔</button>
				<button @click="lineWidth=4" size="mini">画笔粗4</button>
				<button @click="lineWidth=8" size="mini">画笔粗8</button>
			</view>
			<view>画布设置</view>
			<view class="btnBox">
				<button @click="bgColor='#fff'" size="mini">白色背景</button>
				<button @click="bgColor='#9cd8ff'" size="mini">蓝色背景</button>
				<button @click="thumb='',$refs.cSign.clear()" size="mini">清空画布</button>
				<button @click="save()" size="mini">保存为图片</button>
			</view>
		</view>
		<image :src="thumb" v-if="thumb" style="width: 750rpx;height: 750rpx;" mode=""></image>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				bgColor:'#fff',
				lineColor:'#000000',
				lineWidth:4,
				thumb:''
			}
		},
		methods: {
			save(){
				this.$refs.cSign.save().then(res=>{
					console.log(res);
					this.thumb = res.tempFilePath
				}).catch(err=>{
					uni.showToast({
						title:err.errMsg,
						icon:'none'
					})
				})
			}
		}
	}
</script>

<style lang="scss">
	.page{
		width: 100vw;
		overflow-x: hidden;
	}
	.content{
		padding: 20rpx 14rpx;
		font-size: 28rpx;
	}
	.btnBox{
		width: 100%;
		display: flex;align-items: center;
		margin-top: 20rpx;
		margin-bottom: 30rpx;
	}
</style>
```


### 二、Props

| 字段				| 类型		| 必填	| 默认值				| 描述																																							|
| -----------		| --------	| ----	| ----------------------| -------------------------------	
| canvasId	| String| 否|  'c'+uuid(18)	| canvasId 画布id 可不填此项|
| width		| String| 否|  750rpx		| 图像宽度 单位rpx/px		|
| height	| String| 否|  750rpx		| 图像高度 单位rpx/px		|
| lineWidth	| Number| 否|4				| 画笔宽度					|
| lineColor	| String| 否|  #000000		| 画笔颜色					|
| bgColor	| String| 否|  #ffffff		| 画布背景颜色				|

### 三、Methods
| 字段		| 描述					|
| ---------	| ------------------------|
| save	| 保存为图片	promise返回值{errMsg: "canvasToTempFilePath:ok",tempFilePath:'图片路径'}			|
| clear	| 清空画布			|