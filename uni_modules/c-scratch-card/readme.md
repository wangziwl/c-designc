# c-scratch-card

### 
- c-scratch-card 刮刮卡

### c-design交流群号：330647926

### 示例预览
[https://static-mp-eeab6da6-80cd-4e80-844a-66b2a7203834.next.bspapp.com/cdesign/index.html#/pagesMarketing/scratchCard/scratchCard](https://static-mp-eeab6da6-80cd-4e80-844a-66b2a7203834.next.bspapp.com/cdesign/index.html#/pagesMarketing/scratchCard/scratchCard)

### 一、使用示例
```html
<template>
	<view>
		<c-scratch-card ref="cScratchCardRef" @open="onOpen" @ready="onReady">
			<view  style="display: flex;align-items: center;justify-content: center;width: 100%;height: 100%;">
				恭喜中奖 iPhone 15Pro MAx
			</view>
		</c-scratch-card>
		<view class="content">
			<view class="btnBox">
				<button @click="$refs.cScratchCardRef.init()" size="mini">重置</button>
			</view>
		</view>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				show:false
			};
		},
		methods:{
			onOpen(){
				console.log('图层已经刮开一半');
			},
			onReady(){
				console.log('ready');
			}
		}
	}
</script>

<style lang="scss">
.content{
		padding: 20rpx 14rpx;
		font-size: 28rpx;
	}
	.btnBox{
		width: 100%;
		display: flex;align-items: center;
		margin-top: 20rpx;
		margin-bottom: 30rpx;
	}
</style>
```




### 二、Props

| 字段			| 类型		| 必填	| 默认值				| 描述										|
| -----------	| --------	| ----	| ----------------------| -------------------------------			|
| canvasId		| String	| 否	|  'c' + uuid(18)		| 画布id									|
| lineWidth		| Number	| 否	|  20					| 画笔宽度									|
| width			| String	| 否	|  750rpx				| 图像宽度 单位rpx/px						|
| height		| String	| 否	|  750rpx				| 图像高度 单位rpx/px						|
| text			| String	| 否	|  刮刮卡				| 涂层上的文字 设置图层图片时失效			|
| textSize		| Number	| 否	|  24					| 涂层上的文字的大小	设置图层图片时失效	|
| textColor		| String	| 否	|  #ffffff				| 涂层上的文字的颜色 设置图层图片时失效		|
| bgColor		| String	| 否	|  #666666				| 涂层颜色 设置图层图片时失效				|
| bgSrc			| String	| 否	|						| 涂层图片地址								|
| disabled		| Boolean	| 否	|		false			| 是否禁止刮开涂层							|
| prizeName		| String	| 否	|						| 奖品名称	可通过插槽替换该区域			|

### 三、Event
| 字段		| 描述						|
| ---------	| ------------------------	|
| ready		| 初始化完成				|
| open		| 被刮开一半				|



### 四、Methods
| 字段		| 描述						|
| ---------	| ------------------------	|
| init		|	重新初始化/重置				|

### 四、Slot
| 字段		| 描述						|
| ---------	| ------------------------	|
| default	|	奖品区域				|