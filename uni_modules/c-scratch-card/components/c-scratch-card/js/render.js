import getfile from './getfile.js'
export default {
	data() {
		return {}
	},
	methods: {
		async render(val, oldValue, vm) {
			let data = val.props,
				myCanvasId = val.myCanvasId
				
			if (this.canvas) {
				this.canvas.height = this.canvas.height; //清空画布
				if(data.bgSrc){
					this.ctx.drawImage(this.corve, 0, 0, this.canvas.width, this.canvas.height) //绘制图片
					vm.callMethod('ready')
				}else{
					this.ctx.fillStyle = data.bgColor;
					this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
					this.ctx.font = `normal 400 ${data.textSize}px sans-serif`;
					this.ctx.fillStyle = data.textColor;
					this.ctx.fillText(data.text, (this.canvas.width - this.ctx.measureText(data.text).width) / 2, this.canvas.height / 2);
					vm.callMethod('ready')
				}
			} else {

				let ggkbox = document.querySelector('.c-scratch-card')
				let ggkboxRect = ggkbox.getBoundingClientRect()
				let canvasGgk = document.querySelector('#' + myCanvasId)
				let canvas = document.createElement('canvas')
				canvas.width = ggkbox.offsetWidth
				canvas.height = ggkbox.offsetHeight
				canvasGgk.append(canvas)



				let ctx = canvas.getContext('2d')
				this.ctx = ctx
				this.canvas = canvas
				let isDraw = false
				canvas.ontouchstart = () => {
					if (!data.disabled) isDraw = true
				}
				canvas.ontouchmove = (e) => {
					if (isDraw) {
						let x = e.targetTouches[0].pageX - ggkboxRect.left
						let y = e.targetTouches[0].pageY - ggkboxRect.top
						ctx.globalCompositeOperation = 'destination-out'
						ctx.beginPath()
						ctx.arc(x, y, 20, 0, 2 * Math.PI)
						ctx.fill()
					}
				}
				canvas.ontouchend = () => {
					isDraw = false
					// 得到canvas的全部数据
					let x = 0,
						y = 0;
					let a = this.ctx.getImageData(x, y, canvas.width, canvas.height);
					// ctx.putImageData(a,0,0);
					let j = 0;
					for (let i = 3; i < a.data.length; i += 4) {
						if (a.data[i] == 0) j++;
					}
					// 当被刮开的区域等于一半时，则可以开始处理结果
					if (j >= a.data.length / 8) {
						// console.log(this.vm);
						vm.callMethod('open')
					}
				}
				canvas.onmousedown = () => {
					isDraw = true
				}
				canvas.onmousemove = (e) => {
					if (isDraw) {
						let x = e.pageX - ggkbox.offsetLeft
						let y = e.pageY - ggkbox.offsetTop
						ctx.globalCompositeOperation = 'destination-out'
						ctx.arc(x, y, 20, 0, 2 * Math.PI)
						ctx.fill()
					}

				}
				canvas.onmouseup = () => {
					isDraw = false
				}
				if(data.bgSrc){
					let img = new Image(); // 创建一个<img>元素
					img.src = await getfile(data.bgSrc); // 设置图片源地址
					img.onload = async () => {
						this.ctx.drawImage(img, 0, 0, canvas.width, canvas.height) //绘制图片
					}
					this.corve = img
					vm.callMethod('ready')
				}else{
					ctx.fillStyle = data.bgColor;
					ctx.fillRect(0, 0, canvas.width, canvas.height);
					ctx.font = `normal 400 ${data.textSize}px sans-serif`;
					ctx.fillStyle = data.textColor;
					ctx.fillText(data.text, (canvas.width - ctx.measureText(data.text).width) / 2, canvas.height / 2);
					vm.callMethod('ready')
				}
				
			}

			// ctx.fillStyle = '#666'
			// ctx.fillRect(0, 0, 300, 255)
			// ctx.font = '24px 微软雅黑'
			// ctx.fillStyle = '#fff'
			// ctx.fillText('刮刮卡', 65, 60)



			// let arr = [{content:'一等奖',p:0.1},{content:'二等奖',p:0.2},{content:'三等奖',p:0.3}]
			// function getjp(arr){
			// 	let random = Math.random()
			// 	for(let item of arr){
			// 		if(random < item.p){
			// 			return item.content
			// 		}
			// 	}
			// }
			// jptext.innerHTML = getjp(arr)||'谢谢惠顾'


		}
	},
	mounted() {
		// console.log(this.width);
	},
}