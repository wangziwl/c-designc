# c-progress-circle

### 
- c-progress-circle 环形进度条

### c-design交流群号：330647926

### 示例预览
[https://cloud.vuedata.wang/cdesign/#/pages/progress-circle/progress-circle](https://cloud.vuedata.wang/cdesign/#/pages/progress-circle/progress-circle)

### 一、使用示例
```html
<template>
	<view class="content">
		<c-progress-circle :progress='progress' :color='color' :size='size' :boderWidth="boderWidth"></c-progress-circle>
		<view class="btnBox">
			<button size="mini" @click="add">+</button>
			<view style="width: 100rpx;text-align: center;">
				{{progress}}
			</view>
			<button size="mini" @click="reduce" type="default">-</button>
		</view>
		<view class="btnBox" style="padding-left: 100rpx;">边框颜色</view>
		<view class="btnBox">
			<button size="mini" @click="color='red'">红色</button>
			<button size="mini" @click="color='green'">绿色</button>
			<button size="mini" @click="color='orange'">橙色</button>
		</view>
		<view class="btnBox" style="padding-left: 100rpx;">进度条尺寸</view>
		<view class="btnBox">
			<button size="mini" @click="size='200rpx'">200rpx</button>
			<button size="mini" @click="size='400rpx'">400rpx</button>
			<button size="mini" @click="size='600rpx'">600rpx</button>
		</view>
		<view class="btnBox" style="padding-left: 100rpx;">边框宽度</view>
		<view class="btnBox">
			<button  size="mini" @click="boderWidth='200rpx'">200rpx</button>
			<button  size="mini" @click="boderWidth='400rpx'">400rpx</button>
			<button  size="mini" @click="boderWidth='600rpx'">600rpx</button>
		</view>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				progress: 0.2,
				color:'red',
				size:'200rpx',
				boderWidth:'200rpx'
			}
		},
		methods: {
			add() {
				if(this.progress<1){
					this.progress +=0.1
					this.progress=this.progress.toFixed(1)*1
				}
			},
			reduce(){
				if(this.progress>0){
					this.progress -=0.1
					this.progress=this.progress.toFixed(1)*1	
				}
			}
		},
	}
</script>

<style lang="scss">
	.content{
		display: flex;
		flex-direction: column;
		align-items: center;
		padding-top: 20rpx;
		font-size: 28rpx;
	}
	.btnBox{
		width: 100%;
		display: flex;
		align-items: center;
		margin-top:30rpx;
	}
</style>
```




### 二、属性介绍

| 字段			| 类型		| 必填	| 默认值				| 描述								|
| -----------	| --------	| ----	| ----------------------| -------------------------------	|
| progress		| Number	| 否	|  0					| 进度 0-1							|
| color			| String	| 否	|  #3ec3c1				| 进度条颜色						|
| size			| String	| 否	|  200rpx				| 进度条尺寸 单位rpx				|
| boderWidth	| String	| 否	|  200rpx				| 边框宽度 单位rpx					|
