# c-ai

### 
- c-ai 基于uni-ai的云端一体模板 低耦合 所见即所得 导入即用 

### c-design交流群号：330647926

### 示例预览

[https://cloud.vuedata.wang/cdesign/#/uni_modules/c-ai/pages/chat/chat](https://cloud.vuedata.wang/cdesign/#/uni_modules/c-ai/pages/chat/chat)
####
![微信小程序预览](https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/f37f24ed-d0fb-44da-8be4-b2a6ea7efe93.png?x-oss-process=image/resize,m_fixed,w_240)

### 注意：在APP端使用需在pages.json中配置键盘弹出方式 "softinputMode": "adjustResize""softinputNavBar": "none"
```
"subPackages": [{
		"root": "uni_modules/c-ai/pages",
		"pages": [{
			"path": "chat/chat",
			"style": {
				"navigationBarTitleText": "C-AI",
				"enablePullDownRefresh": false,
				"backgroundColor": "#ffffff",
				"app-plus": {
					"softinputMode": "adjustResize",
					"softinputNavBar": "none"
				}
			}

		}]
}]
```
### [uni-ai文档](https://uniapp.dcloud.net.cn/uniCloud/uni-ai.html)