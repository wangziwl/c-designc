"use strict";
const common_vendor = require("../../../../common/vendor.js");
let ctx = null, thumb = null;
const _sfc_main = {
  name: "c-canvas",
  props: {
    canvasId: {
      type: String,
      default: "myCanvas"
    },
    isAuto: {
      type: Boolean,
      default: true
    },
    //画布宽度
    width: {
      type: Number,
      default: 375
    },
    //画布高度
    height: {
      type: Number,
      default: 375
    },
    drawData: {
      type: Array,
      default: () => {
        return [];
      }
    }
  },
  emits: ["drawSuccess"],
  data() {
    return {};
  },
  methods: {
    drawText(item) {
      if (item.font)
        ctx.font = item.font;
      if (item.color)
        ctx.fillStyle = item.color;
      if (item.textAlign && item.x && item.y)
        ctx.textAlign = item.textAlign;
      if (item.value)
        this.textPrewrap(ctx, item.value, item.x || 0, item.y || 0, item.lineHeight || 20, item.lineMaxWidth || 200, item.lineNum || 1);
    },
    async drawImage(item) {
      let Img = item.value;
      if (item.radius) {
        await this.drawRoundRect(item.radius, item.x || 0, item.y || 0, item.width, item.height, Img);
      } else {
        let imgBit = await this.loadImg(Img);
        ctx.drawImage(imgBit, item.x || 0, item.y || 0, item.width, item.height);
      }
    },
    loadImg(src) {
      return new Promise((resolve, reject) => {
        let imgBit = this.canvas.createImage();
        imgBit.src = src;
        imgBit.onload = (e) => {
          resolve(imgBit);
        };
      });
    },
    /*
    	 *  参数说明
    	 *  ctx Canvas实例
    	 *  img 图片地址
    	 *   x  x轴坐标
    	 *   y  y轴坐标
    	 *   w  宽度
    	 *   h  高度
    	 *   r  弧度大小
    	 */
    async drawRoundRect(r, x, y, w, h, img) {
      ctx.save();
      if (w < 2 * r)
        r = w / 2;
      if (h < 2 * r)
        r = h / 2;
      ctx.beginPath();
      ctx.moveTo(x + r, y);
      ctx.arcTo(x + w, y, x + w, y + h, r);
      ctx.arcTo(x + w, y + h, x, y + h, r);
      ctx.arcTo(x, y + h, x, y, r);
      ctx.arcTo(x, y, x + w, y, r);
      ctx.closePath();
      ctx.clip();
      let imgBit = await this.loadImg(img);
      ctx.drawImage(imgBit, x, y, w, h);
      ctx.restore();
    },
    getContext() {
      return new Promise((resolve) => {
        const { pixelRatio } = common_vendor.index.getSystemInfoSync();
        common_vendor.index.createSelectorQuery().in(this).select(`#${this.canvasId}`).fields({ node: true, size: true }).exec((res) => {
          const { width, height } = res[0];
          const canvas = res[0].node;
          canvas.width = res[0].width * pixelRatio;
          canvas.height = res[0].height * pixelRatio;
          resolve({ canvas, width, height, pixelRatio });
        });
      });
    },
    async draw() {
      if (thumb) {
        this.$emit("drawSuccess", thumb);
        return;
      }
      const { canvas, pixelRatio } = await this.getContext();
      this.canvas = canvas;
      ctx = canvas.getContext("2d");
      ctx.scale(pixelRatio, pixelRatio);
      for (let item of this.drawData) {
        if (item.type == "text") {
          this.drawText(item);
        } else if (item.type == "image") {
          await this.drawImage(item);
        }
      }
      setTimeout(() => {
        common_vendor.index.canvasToTempFilePath({
          canvas: this.canvas,
          quality: 1,
          success: (ret) => {
            thumb = ret.tempFilePath;
            this.$emit("drawSuccess", thumb);
          },
          fail: (err) => {
            console.log(err);
          }
        });
      }, 80);
    },
    checkUrl(url) {
      return /(http|https):\/\/([\w.]+\/?)\S*/.test(url);
    },
    textPrewrap(ctx2, content, drawX, drawY, lineHeight, lineMaxWidth, lineNum) {
      let drawTxt = "";
      let drawLine = 1;
      let drawIndex = 0;
      if (ctx2.measureText(content).width <= lineMaxWidth) {
        ctx2.fillText(content, drawX, drawY);
      } else {
        for (let i = 0; i <= content.length; i++) {
          drawTxt += content[i];
          if (drawLine === lineNum && i == content.length) {
            if (ctx2.measureText(drawTxt).width > lineMaxWidth) {
              ctx2.fillText(content.substring(drawIndex, i) + "...", drawX, drawY);
            } else {
              ctx2.fillText(content.substring(drawIndex, i), drawX, drawY);
            }
          } else {
            if (ctx2.measureText(drawTxt).width > lineMaxWidth) {
              ctx2.fillText(content.substring(drawIndex, i + 1), drawX, drawY);
              drawIndex = i + 1;
              drawLine += 1;
              drawY += lineHeight;
              drawTxt = "";
            }
          }
        }
      }
    }
  },
  mounted() {
    if (this.isAuto)
      this.draw();
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $props.canvasId,
    b: $props.canvasId,
    c: `${$props.width}px`,
    d: `${$props.height}px`
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
