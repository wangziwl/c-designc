"use strict";
const common_vendor = require("../../../../common/vendor.js");
const common_assets = require("../../../../common/assets.js");
const _sfc_main = {
  props: {
    screenData: {
      type: Array,
      default: () => []
    }
  },
  emits: ["confirm"],
  data() {
    return {
      top: common_vendor.index.getSystemInfoSync().statusBarHeight + 18,
      isChange: false,
      islongPress: false
      //长按记录变量
    };
  },
  methods: {
    longpress() {
      this.islongPress = true;
      this.isChange = true;
    },
    touchend() {
      setTimeout(() => {
        this.islongPress = false;
      }, 200);
    },
    click(item) {
      if (this.islongPress == false) {
        if (this.isChange)
          this.$emit("confirm", item);
        this.isChange = false;
      } else if (this.islongPress == true)
        ;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($props.screenData, (item, index, i0) => {
      return {
        a: item.value,
        b: common_vendor.o((...args) => $options.longpress && $options.longpress(...args), index),
        c: common_vendor.o(($event) => $options.click(item), index),
        d: common_vendor.o((...args) => $options.touchend && $options.touchend(...args), index),
        e: index
      };
    }),
    b: common_assets._imports_0$1,
    c: common_assets._imports_1$1,
    d: common_assets._imports_2$1,
    e: common_assets._imports_3,
    f: ($data.isChange ? 16 : $data.top) + "px",
    g: $data.isChange ? 1 : "",
    h: !$data.isChange,
    i: $data.isChange ? "100rpx" : "0",
    j: $data.isChange ? "100rpx" : "0"
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
