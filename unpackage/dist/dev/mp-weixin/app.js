"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/lockScreen/lockScreen.js";
  "./pages/canvas/canvas.js";
  "./pages/progress-circle/progress-circle.js";
  "./pages/svga/svga.js";
  "./pages/lottie/lottie.js";
  "./pages/3d-model/3d-model.js";
  "./pages/sign/sign.js";
  "./uni_modules/c-ai/pages/chat/chat.js";
  "./pagesMarketing/scratchCard/scratchCard.js";
}
const _sfc_main = {
  onLaunch: function() {
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
function createApp() {
  const app = common_vendor.createSSRApp(_sfc_main);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
