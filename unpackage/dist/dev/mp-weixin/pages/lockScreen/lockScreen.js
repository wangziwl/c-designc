"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      screenData: [
        { id: 2, value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/4f802abc-4b7e-47f7-9e64-b5a71e836582.jpg" },
        { id: 3, value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/d9bb2dc4-fe37-4719-8415-a642ae763258.jpg" },
        { id: 1, value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/57275575-4a36-4c6d-8ea4-320cfc89662a.jpg" }
      ]
    };
  },
  methods: {
    confirm(e) {
      console.log(e);
    }
  }
};
if (!Array) {
  const _easycom_c_lockScreen2 = common_vendor.resolveComponent("c-lockScreen");
  _easycom_c_lockScreen2();
}
const _easycom_c_lockScreen = () => "../../uni_modules/c-lockScreen/components/c-lockScreen/c-lockScreen.js";
if (!Math) {
  _easycom_c_lockScreen();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o($options.confirm),
    b: common_vendor.p({
      screenData: $data.screenData
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
