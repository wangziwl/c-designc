"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      thumb: "",
      canvasId: "myCanvas",
      width: 375,
      height: 475,
      drawData: [
        {
          type: "image",
          x: 0,
          y: 0,
          value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/f5c141ce-472b-4ce7-8886-2028ca631e0c.png",
          width: 375,
          height: 475
        },
        {
          type: "image",
          x: 0,
          y: 0,
          value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/f6bd3c29-d452-423e-a4a6-1728b9c2b074.jpeg",
          width: 375,
          height: 375,
          radius: 10
        },
        {
          type: "text",
          x: 10,
          y: 400,
          value: "22新款M2Apple/苹果 MacBook Air 13 英寸 MacBook Air笔记本电脑",
          color: "#262626",
          font: "normal 400 14px sans-serif",
          lineHeight: 20,
          lineMaxWidth: 250,
          lineNum: 2
        },
        {
          type: "image",
          x: 280,
          y: 385,
          value: "https://mp-eeab6da6-80cd-4e80-844a-66b2a7203834.cdn.bspapp.com/cloudstorage/7dca3f02-baef-470d-88d2-eebd7c31ab73.jpg",
          width: 80,
          height: 80
        },
        {
          type: "text",
          x: 10,
          y: 450,
          value: "¥ 9999.00",
          color: "#f44",
          font: "normal 400 24px sans-serif",
          lineHeight: 20,
          lineMaxWidth: 200,
          lineNum: 2
        }
      ]
    };
  },
  onReady() {
  }
};
if (!Array) {
  const _easycom_c_canvas2 = common_vendor.resolveComponent("c-canvas");
  _easycom_c_canvas2();
}
const _easycom_c_canvas = () => "../../uni_modules/c-canvas/components/c-canvas/c-canvas.js";
if (!Math) {
  _easycom_c_canvas();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.sr("cCanvas", "795f6b8f-0"),
    b: common_vendor.o(($event) => $data.thumb = $event),
    c: common_vendor.p({
      drawData: $data.drawData,
      width: 375,
      height: 475
    }),
    d: $data.thumb
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
