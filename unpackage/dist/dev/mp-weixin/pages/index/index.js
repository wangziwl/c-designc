"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const _sfc_main = {
  data() {
    return {
      tabIndex: 0,
      tabs: [
        {
          id: 1,
          name: "基础组件",
          componentList: [
            {
              name: "电子签名",
              url: "/pages/sign/sign"
            },
            {
              name: "3d-model",
              url: "/pages/3d-model/3d-model"
            },
            {
              name: "lottie",
              url: "/pages/lottie/lottie"
            },
            {
              name: "svga",
              url: "/pages/svga/svga"
            },
            {
              name: "海报",
              url: "/pages/canvas/canvas"
            },
            {
              name: "环形进度条",
              url: "/pages/progress-circle/progress-circle"
            },
            {
              name: "锁屏",
              url: "/pages/lockScreen/lockScreen"
            }
          ]
        },
        {
          id: 2,
          name: "AI组件",
          componentList: [{
            name: "C-AI",
            url: "/uni_modules/c-ai/pages/chat/chat"
          }]
        },
        {
          id: 3,
          name: "营销组件",
          componentList: [{
            name: "刮刮卡",
            url: "/pagesMarketing/scratchCard/scratchCard"
          }]
        }
      ]
    };
  },
  computed: {
    componentList() {
      return this.tabs[this.tabIndex].componentList;
    }
  },
  methods: {
    gourl(url) {
      common_vendor.index.navigateTo({
        url
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_assets._imports_0,
    b: common_vendor.f($data.tabs, (item, index, i0) => {
      return {
        a: common_vendor.t(item.name),
        b: index == $data.tabIndex ? 1 : "",
        c: item.id,
        d: common_vendor.o(($event) => $data.tabIndex = index, item.id)
      };
    }),
    c: common_assets._imports_1,
    d: common_vendor.f($options.componentList, (item, index, i0) => {
      return {
        a: common_vendor.t(item.name),
        b: index,
        c: common_vendor.o(($event) => $options.gourl(item.url), index)
      };
    }),
    e: common_assets._imports_2,
    f: $options.componentList.length > 0 ? 1 : "",
    g: $options.componentList.length == 0 ? 1 : ""
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
